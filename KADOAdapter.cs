using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

/*
    KADO
    Copyright (C) 2018 Teh_Fakwer

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/

namespace KADO
{
    public class KADOAdapter : IDisposable
    {
        public string ConnectionString;
        public KADOAdapter(string connectionString)
        {
            ConnectionString = connectionString;
        }        
        public KADOAdapter()
        {
            
        }

        #region CRUD        
        public T Add<T>(T entity)
            where T : class
        {
            string tableName = typeof(T).UnderlyingSystemType.Name;
            string primaryKeyColumnName = GetPrimaryKeyColumnName(tableName);
            var dt = GetTableTemplate<T>();
            var propertyNames = dt.Columns.Cast<DataColumn>().Where(c => c.ColumnName != primaryKeyColumnName).Select(c => c.ColumnName).ToArray();

            var parameterProperties = propertyNames.Select(x => "@" + x);

            string parameters = string.Join(" ,", parameterProperties);
            string columnNames = string.Join(" ,", propertyNames);

            string updateCommand = string.Format("DECLARE @IdTable TABLE(Id INT) INSERT INTO dbo.{0}({1}) OUTPUT INSERTED.{2} INTO @IdTable Values({3}) SELECT Id FROM @IdTable", tableName, columnNames, primaryKeyColumnName, parameters);

            using (var con = new SqlConnection(ConnectionString))
            {
                using (var cmd = new SqlCommand(updateCommand, con))
                {
                    con.Open();

                    foreach (string property in propertyNames)
                    {

                        var value = typeof(T).GetProperty(property).GetValue(entity);
                        if (value == null)
                            cmd.Parameters.AddWithValue("@" + property, DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@" + property, value);
                    }

                    var dataTable = new DataTable(primaryKeyColumnName);
                    new SqlDataAdapter(cmd).Fill(dataTable);

                    var idProperty = typeof(T).GetProperty(primaryKeyColumnName);
                    var idValue = dataTable.Rows[0][0] == DBNull.Value ? null : dataTable.Rows[0][0];
                    idProperty.SetValue(entity, idValue);
                }
            }
            return entity;
        }
        public void AddBulk<T>(IList<T> entities)
            where T : class
        {
            var dataTable = GetTableTemplate<T>();

            var properties = typeof(T).GetProperties().Select(x => x.Name).ToArray();

            for(int i = 0; i < entities.Count; ++i)
            {
                dataTable.Rows.Add();
                for(int k = 0; k < properties.Count(); ++k)
                {
                    var property = typeof(T).GetProperty(properties[k]);
                    var value = property.GetValue(entities[i]);
                    dataTable.Rows[0][properties[k]] = value ?? (object)DBNull.Value;
                }
            }
            AddBulk<T>(dataTable);
        }
        public void AddBulk<T>(DataTable dataTable)
            where T : class
        {

            using (var connection = new SqlConnection(ConnectionString))
            {
                string name = typeof(T).UnderlyingSystemType.Name;
                connection.Open();
                using (var bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName = "dbo." + name;

                    try
                    {
                        bulkCopy.BulkCopyTimeout = 180;
                        bulkCopy.WriteToServer(dataTable);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        throw new Exception(string.Format("An error occurred when trying to insert records into the {0} table. ", name));
                    }


                }

            }
        }
        public IList<T> AddRange<T>(IList<T> entities)
            where T : class
        {
            var dataTable = GetTableTemplate<T>();

            var properties = typeof(T).GetProperties().Select(x => x.Name).ToArray();

            for (int i = 0; i < entities.Count; ++i)
            {
                entities[i] = Add(entities[i]);
            }

            return entities;
        }        
        public void Update<T>(T entity)
            where T : class, new()
        {
            string tableName = typeof(T).UnderlyingSystemType.Name;
            string primaryKeyColumnName = GetPrimaryKeyColumnName(tableName);
            var dt = GetTableTemplate<T>();
            var propertyNames = dt.Columns.Cast<DataColumn>().Where(c => c.ColumnName != primaryKeyColumnName).Select(c => c.ColumnName);

            var parameters = propertyNames.Select(x => x + " = @" + x);

            string setQuery = string.Join(" ,", parameters);

            string updateCommand = string.Format("Update dbo.{0} Set {1} WHERE {2} = @P_KEY", tableName, setQuery, primaryKeyColumnName);

            using (var con = new SqlConnection(ConnectionString))
            {
                using (var cmd = new SqlCommand(updateCommand, con))
                {
                    con.Open();

                    foreach (string property in propertyNames)
                    {

                        var value = typeof(T).GetProperty(property).GetValue(entity);
                        if (value == null)
                            cmd.Parameters.AddWithValue("@" + property, DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@" + property, value);
                    }

                    var pkey = typeof(T).GetProperty(primaryKeyColumnName).GetValue(entity);

                    if (primaryKeyColumnName == "Id")
                    {
                        cmd.Parameters.AddWithValue("@P_KEY", (int)pkey);
                    }
                    else
                        cmd.Parameters.AddWithValue("@P_KEY", (string)pkey);

                    int inserted = cmd.ExecuteNonQuery();
                }
            }
        }
        public void Delete<T>(T entity)
        {
            string name = typeof(T).UnderlyingSystemType.Name;
            string pkey = GetPrimaryKeyColumnName(name);
            var idProperty = typeof(T).GetProperty(pkey);
            var idValue = idProperty.GetValue(entity);

            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                var sqlCommand = string.Format("DELETE FROM {0} WHERE {1} = @Id", name, pkey);

                using (var cmd = new SqlCommand(sqlCommand, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@Id", idValue);
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        throw new Exception(string.Format("An error occurred when trying to delete the record from the {0} table. ", name));
                    }
                }
            }
        }
        public T GetEntityById<T>(int id)
            where T : class, new()
        {
            var name = typeof(T).UnderlyingSystemType.Name;
            var sqlCommand = string.Format("SELECT * FROM {0} WHERE Id = {1}", name, id);
            var dataTable = GetTableFromSql(sqlCommand);

            if (dataTable.Columns.Count > 0)
            {
                var entities = GetListFromDataTable<T>(dataTable);
                var entity = entities.FirstOrDefault();
                return entity;
            }
            return null;
        }
        public DataTable GetTableTemplate<T>()
            where T : class
        {
            var name = typeof(T).UnderlyingSystemType.Name;
            var sqlCommand = string.Format("SELECT * FROM {0} WHERE 1=0", name);
            var dataTable = GetTableFromSql(sqlCommand);
            return dataTable;
        }
        public void ExecuteSql(string sqlCommand, SqlParameter[] parameters = null)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand(sqlCommand, conn))
                {
                    try
                    {
                        if (parameters != null)
                            foreach (var p in parameters)
                                cmd.Parameters.Add(p);
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex);
                        throw ex;
                    }
                }
            }
        }
        public IList<T> GetList<T>()
          where T : class, new()
        {
            var name = typeof(T).UnderlyingSystemType.Name;
            var sqlCommand = string.Format("SELECT * FROM {0}", name);
            var dataTable = GetTableFromSql(sqlCommand);
            
            if (dataTable.Rows.Count > 0)
            {
                IList<T> entities = GetListFromDataTable<T>(dataTable);
                return entities;
            }
            return null;
        }        
        public IList<T> GetListFromSql<T>(string sqlCommand, SqlParameter[] parameters = null)
          where T : class, new()
        {
            var name = typeof(T).UnderlyingSystemType.Name;

            DataTable dataTable = GetTableFromSql(sqlCommand, parameters);
            
            if (dataTable.Rows.Count > 0)
            {
                IList<T> entities = GetListFromDataTable<T>(dataTable);
                return entities;
            }
            return null;
        }
        public DataTable GetTableFromSql(string sqlCommand, SqlParameter[] parameters = null)
        {
            var dataTable = new DataTable();

            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand(sqlCommand, conn))
                {
                    try
                    {
                        if (parameters != null)
                            foreach (var p in parameters)
                                cmd.Parameters.Add(p);
                        var dataAdapter = new SqlDataAdapter(cmd);
                        dataAdapter.Fill(dataTable);
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex);
                        throw ex;
                    }
                }
            }
            return dataTable;
        }        
        #endregion

        #region Helper Functions
        private IList<T> GetListFromDataTable<T>(DataTable dataTable)
          where T : class, new()
        {
            IList<string> properties;
            IList<T> entities;
            if (dataTable.Columns.Count > 0)
            {
                try
                {
                    properties = new List<string>();
                    entities = new List<T>();
                    for (int i = 0; i < dataTable.Columns.Count; ++i)
                    {
                        properties.Add(typeof(T).GetProperty(dataTable.Columns[i].ColumnName).Name);
                    }
                    for (int r = 0; r < dataTable.Rows.Count; ++r)
                    {
                        T entity = new T();
                        for (int c = 0; c < properties.Count; ++c)
                        {
                            var property = typeof(T).GetProperty(properties[c]);
                            var columnType = dataTable.Rows[r][c].GetType();
                            var value = dataTable.Rows[r][c] == DBNull.Value ? null : dataTable.Rows[r][c];
                            property.SetValue(entity, value);
                        }
                        entities.Add(entity);
                    }
                    return entities;
                }
                catch
                {
                    string name = typeof(T).UnderlyingSystemType.Name;
                    throw new Exception("Error occurred when attempting to serialize the data table to object models.");
                }
            }
            return null;

        }        
        string GetPrimaryKeyColumnName(string name)
        {
            var primaryKey = new DataTable();
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                string command = "SELECT Col.Column_Name as PrimaryKey FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col WHERE Col.Constraint_Name = Tab.Constraint_Name AND Col.Table_Name = Tab.Table_Name AND Constraint_Type = 'PRIMARY KEY' AND Col.TABLE_NAME = @Name";

                using (var cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@Name", name);
                    try
                    {
                        var dataAdapter = new SqlDataAdapter(cmd);
                        dataAdapter.Fill(primaryKey);
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex);
                        throw new Exception(string.Format("An error occurred when attempting to see if fields are nullable for table: {0}.", name));
                    }
                }
            }
            return (string)primaryKey.Rows[0][0];
        }                
        #endregion 

        #region Functions required to use using statements
        bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        ~KADOAdapter()
        {
            Dispose(false);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {

            }
            _disposed = true;
        }
        #endregion
    }
}

KADO.txt
Displaying KADO.txt.
